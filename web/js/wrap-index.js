$(document).ready(function() {

  // Sidenav Mobile //
  $('.sidenav').sidenav();
  
  // Carousel //

  // Efeito Carrosel
  $('.carousel').carousel({
    dist: 0
  });

  // Efeito Autoplay
  window.setInterval(function () {
    $('.carousel').carousel('next');
  }, 6000);

  // Efeito Slider
  $('.slider').slider();
  
});